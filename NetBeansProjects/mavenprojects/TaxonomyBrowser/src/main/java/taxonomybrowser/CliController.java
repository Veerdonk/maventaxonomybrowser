/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxonomybrowser;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * CliController takes care of parsing all commandline arguments
 * @author dvandeveerdonk
 */
public class CliController {
    public static Options constructGnuOptions(){
    final Options gnuOptions = new Options();
    gnuOptions.addOption("z", "zip", true, "Path to input zipfile.")
              .addOption("h","help", false, "Display helpful information.")
              .addOption("sm", "summary", false, "Display a summary of the zipfile.")
              .addOption("t", "tax_id", true, "Retrieve ifo from organism with this tax_id.")
              .addOption("lc", "list_children", true, "List all children of organism with given tax_id.")
              .addOption("s", "sort", true, "Sorts children by tax_id/rank/name/number of child nodes.")
              .addOption("o", "omit_subspecies", false, "Omits subspecies when listing children.")
              .addOption("lr", "list_rank", true, "Lists all species with given rank.")
              .addOption("fl", "fetch_lineage", true, "Fetches the lineage of given tax_id drom root.")
              .addOption("l", "limit", true, "Limits the search for lineage to n nodes.");
    return gnuOptions;
    }

    /**
     * Used for printing usage options.
     * @param applicationName
     * @param options
     * @param out
     */
    public static void printUsage(
    final String applicationName,
    final Options options,
    final OutputStream out)
    {
        try (PrintWriter writer = new PrintWriter(out)) {
            final HelpFormatter usageFormatter = new HelpFormatter();
            usageFormatter.printUsage(writer, 80, applicationName, options);
        }
    }
    /**
     * Used for printing Helpful information to the commandline
     * @param options
     * @param printedRowWidth
     * @param header
     * @param footer
     * @param spacesBeforeOption
     * @param spacesBeforeOptionDescription
     * @param displayUsage
     * @param out
     */
    public static void printHelp(
    final Options options,
    final int printedRowWidth,
    final String header,
    final String footer,
    final int spacesBeforeOption,
    final int spacesBeforeOptionDescription,
    final boolean displayUsage,
    final OutputStream out)
    {
        final String commandLineSyntax = "java -z taxdmp.zip";
        try (PrintWriter writer = new PrintWriter(out)) {
            final HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp(
                    writer,
                    printedRowWidth,
                    commandLineSyntax,
                    header,
                    options,
                    printedRowWidth,
                    printedRowWidth,
                    footer,
                    displayUsage);
        }
    }

    /**
     * Parses the commandline
     * @param commandLineArguments
     * @throws IOException
     */
    public void useGnuParser(final String[] commandLineArguments) throws IOException {
        final CommandLineParser cmdLineGnuParser = new GnuParser();
        final Options gnuOptions = constructGnuOptions();
        CommandLine commandLine;
        TaxController taxc = new TaxController();
        try {
        commandLine = cmdLineGnuParser.parse(gnuOptions, commandLineArguments);
        if (commandLine.hasOption("z")) {

        }
        else{
            throw new IOException("A zip file must be supplied.");
        }
        TaxCollection taxMap = taxc.getTaxMap(commandLine.getOptionValue("z"));
        if (commandLine.hasOption("h")) {

            System.out.println("--HELP--");
            printHelp(
                    constructGnuOptions(),
                    80,
                    "Helpful info:",
                    "End of help",
                    5,
                    3,
                    true,
                    System.out);
        }
        if(commandLine.hasOption("sm")){
            taxc.summary_hm(taxMap, commandLine.getOptionValue("z"));
        }
        if(commandLine.hasOption("t")){
            try{
                int tax_id = Integer.parseInt(commandLine.getOptionValue("t"));
                taxc.getTaxInfo(taxMap, tax_id);

            }
            catch(NumberFormatException ex){
                System.out.println("Given tax_id is not an integer");
            }
        }
        if(commandLine.hasOption("lc")){

            if(commandLine.hasOption("s")){
                if(commandLine.hasOption("o")){
                    taxc.listChildren(taxMap, Integer.parseInt(commandLine.getOptionValue("lc")), commandLine.getOptionValue("s"), true);
                }
                else{
                    taxc.listChildren(taxMap, Integer.parseInt(commandLine.getOptionValue("lc")), commandLine.getOptionValue("s"), false);
                }
            }
            else{
                if(commandLine.hasOption("o")){
                   taxc.listChildren(taxMap, Integer.parseInt(commandLine.getOptionValue("lc")), "DEFAULT", true);
                }
                else{
                    taxc.listChildren(taxMap, Integer.parseInt(commandLine.getOptionValue("lc")), "DEFAULT", false);
                }
            }

        }

        if(commandLine.hasOption("fl")){
            if(commandLine.hasOption("l")){
                taxc.lineage(taxMap, Integer.parseInt(commandLine.getOptionValue("fl")), Integer.parseInt(commandLine.getOptionValue("l")));
            }
            taxc.lineage(taxMap, Integer.parseInt(commandLine.getOptionValue("fl")), 0);
        }

        }
        catch (ParseException parseException)
        {
            System.err.println(
                    "Encountered exception while parsing commandline:\n"
                    + parseException.getMessage());
        }
    }

    }

